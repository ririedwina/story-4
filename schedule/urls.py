from django.conf.urls import url
from django.urls import include, path
from . import views


urlpatterns = [
    path('', views.schedule, name="schedule"),
    path('show/', views.schedule_list, name="schedule_list"),
    path('show/<pk>/delete/', views.schedule_delete, name = "schedule_delete")
]