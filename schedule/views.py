from django.shortcuts import render, redirect
from .forms import activity_form
from django.http import HttpResponse
from .models import Activity
from django.views import generic
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy

# Create your views here.
def schedule(request):
    if(request.method == 'POST'):
        form = activity_form(request.POST)
        if form.is_valid():
            post = form.save()
            post.save()
        return redirect('/schedule/show/')
    else:
        schedules = activity_form()
        return render (request, "schedule.html", {'schedule_form':schedules})

def schedule_list(request):
    schedules = Activity.objects.all()
    return render(request, "schedule_list.html", {'schedules': schedules})

def schedule_delete(request,pk):
        data = Activity.objects.get(pk=pk)
        if request.method == "POST":
            data.delete()
            return redirect ("schedule_list")

# class schedule_delete(DeleteView):
#     model = Activity
#     deleted = reverse_lazy('schedule:schedule_list')

