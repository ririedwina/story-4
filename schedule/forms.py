from django import forms
from .models import Activity

# class activity_form(forms.Form):
#     activity_name = forms.CharField(max_length = 20, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder':'Enter activity name'}))
#     activity_category = forms.CharField(max_length = 20, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder':'Enter activity category'}))
#     activity_place = forms.CharField(max_length = 20, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder':'Enter activity place'}))
#     activity_date = forms.DateField(widget = forms.DateInput(attrs = {'type':'date'}))
#     activity_time = forms.TimeField(widget = forms.TimeInput(attrs = {'type':'time'}))
    
class activity_form(forms.ModelForm):
    
    class Meta:
        model = Activity
        fields = ['name', 'category', 'place', 'date', 'time' ]
        widget = {
            'date' : forms.DateInput(attrs = {'type':'date'}),
            'time' : forms.TimeInput(attrs = {'type':'time'})
        }