from django.contrib import admin
from django.conf.urls import url
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('webweb.urls')),
    path('schedule/', include('schedule.urls')),
]
