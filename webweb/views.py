from django.http import HttpResponse
from django.shortcuts import render

def aboutme(request):
    return render(request, 'aboutme.html')

def portofolio(request):
    return render(request, 'portofolio.html')

def contact(request):
    return render(request, 'contact.html')

def home(request):
    return render(request, 'Story3.html')