from django.contrib import admin
from django.urls import include, path
from .views import *

urlpatterns = [
    path('', home, name='index'),
    path('aboutme', aboutme, name='about_me'),
    path('contact', contact, name='contact'),
    path('portofolio', portofolio, name='portofolio'),
]
